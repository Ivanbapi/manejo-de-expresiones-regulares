
import java.util.Scanner;

public class Ejercicio2 { 
    
    public static void main(String[] args){
        Scanner s= new Scanner(System.in);
        int contador = 0;
        
        System.out.println("Introduzca la frase.");
        String frase = s.nextLine();
        
            for(int i=0; i<frase.length(); i++){
                 if ((frase.charAt(i)=='a') || (frase.charAt(i)=='e') || (frase.charAt(i)=='i') || (frase.charAt(i)=='o') || (frase.charAt(i)=='u')){ 
                 contador++;
                }
                
            }
            System.out.println("La pablabra "+ frase +" tiene: "+contador+ " vocales");
        
        
    }
    
    
}
