import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EjerciciosExpresionesRegulares {
    //Ejercicio 1---------------------------------------------------------------
    public static boolean busca_3a(String cadena){
        String  expR = ".[aA].[aA].[aA]";
        Pattern pat = Pattern.compile(expR);
        Matcher mat = pat.matcher(cadena);                                                                                   
        return mat.matches();   
    }
    public void main1(){
        System.out.println(busca_3a("mañana"));
        System.out.println(busca_3a("banana"));
        System.out.println(busca_3a("Esto e"));
    }
    //Termina el primer ejercicio
    
    
    //Ejercicio 2---------------------------------------------------------------
    
    public void Ejercicio2(){
        String regexp = "\\d{1,2}/\\d{1,2}/\\d{4}";
        
        //Lo siguiente devuelve true
        System.out.println(Pattern.matches(regexp,"11/12/2014"));
        System.out.println(Pattern.matches(regexp,"1/12/2014"));
        System.out.println(Pattern.matches(regexp,"11/2/2014"));
        
        //Lo siguiente devuelven false
        System.out.println(Pattern.matches(regexp,"1/12/14"));
        System.out.println(Pattern.matches(regexp,"11//2014"));
        System.out.println(Pattern.matches(regexp,"11/12/14pericos"));
        
    }
    //Aqui termina el ejercicio 2
    
    
    //EJERCICIO 3---------------------------------------------------------------
    public void Ejercicio3(){
         String literalMonthRegexp = "\\d{1,2}/(?i)(ene|feb|mar|abr|may|jun|jul|ago|sep|oct|nov|dic)/\\d{4}";
        
        // Lo siguiente devuelve true 
        System.out.println(Pattern.matches(literalMonthRegexp, "11/dic/2014")); 
        System.out.println(Pattern.matches(literalMonthRegexp, "1/nov/2014")); 
        System.out.println(Pattern.matches(literalMonthRegexp, "1/AGO/2014")); // Mes en mayúsculas 
        System.out.println(Pattern.matches(literalMonthRegexp, "21/Oct/2014")); // Primera letra del mes en mayúsculas. 
        // Los siguientes devuelven false 
        System.out.println(Pattern.matches(literalMonthRegexp, "11/abc/2014")); // abc no es un mes 
        System.out.println(Pattern.matches(literalMonthRegexp, "11//2014")); // falta el mes 
        System.out.println(Pattern.matches(literalMonthRegexp, "11/jul/2014perico")); // sobra perico

    }
    //Aqui termina el ejercicio 3
    
    
    //EJERCICIO 4---------------------------------------------------------------
    
    public static boolean validaNumeroEnteroPositivo_Exp(String texto){
           return texto.matches("^[0-9]+$");
        }
    public void main4(){
        System.out.println(validaNumeroEnteroPositivo_Exp("158"));
    }
    //Aqui termina el ejercicio 4
    
    
    //EJERCICIO 5---------------------------------------------------------------
    
     public static boolean validaNumeroEnteroNegativo_Exp(String texto){
            return texto.matches("^-[0-9]+$");
        }
         public void main5(){
             System.out.println(validaNumeroEnteroNegativo_Exp("-10"));
         }
    //Aquí termina el ejercicio 5
         
         
    //EJERCICIO 6---------------------------------------------------------------
         
     public static boolean validaNumeroEntero_Exp(String texto){
            return texto.matches("^-?[0-9]+$");
         }           
         public void main6(){
             System.out.println(validaNumeroEntero_Exp("-10"));
         }
    //Aquí termina el ejercicio 6
         
         
    //EJERCICIO 7---------------------------------------------------------------
         
            public static boolean validarMatriculaEuropea_Exp(String matricula) {
            return matricula.matches("^[0-9]{4}[A-Z]{3}$");  
        } 
        
         public static boolean validarMatriculaUAC_Exp(String matricula) {
            return matricula.matches("^[A-Z]{2}[0-9]{6}$");  
        } 
        
         public void main7(){
             System.out.println(validarMatriculaEuropea_Exp("4587AFT"));
             
             System.out.println(validarMatriculaUAC_Exp("AL058804"));
         }
         //Aqui termina el ejercicio 7
         
         
    //EJERCICIO 8---------------------------------------------------------------
         
          public static boolean validaBinario_Exp(String binario){
            return binario.matches("^[0-1]+$");
        }
        
         public void main8(){
             System.out.println(validaBinario_Exp("1001"));
         }
    //Aquí termina el ejercicio 8
         
         
    //EJERCICIO 9---------------------------------------------------------------
         
          public static boolean validarUsuarioTwitter_Exp(String usuario_twitter) {
            return usuario_twitter.matches("^@([A-Za-z0-9_]{1,15})$");
        }
        
         public void main9(){
             System.out.println(validarUsuarioTwitter_Exp("@ivanbarrios422"));   
         }
    //Aquí termina el ejercicio 9
         
         
    //EJERCICIO 10--------------------------------------------------------------
         
          public void main10(){
             String dniRegexp = "\\d{8}[A-HJ-NP-TV-Z]" ;
             
        // Lo siguiente devuelve true
        System.out.println(Pattern.matches(dniRegexp, "01234567C" ));
        
        // Lo siguiente devuelve faslse
        System.out.println(Pattern.matches(dniRegexp, "01234567U" )); 
        System.out.println(Pattern.matches(dniRegexp, "0123567X")); 
           
         }
    //Aquí termina el ejercicio 10
          public void main11(){
              String emailRegexp = "[^@]+@[^@]+\\.[a-zA-Z]{2,}";
     
       // Lo siguiente devuelve true
        System.out.println(Pattern.matches(emailRegexp, "a@b.com"));
        System.out.println(Pattern.matches(emailRegexp, "+++@+++.com"));
        
       // Lo siguiente devuelve faslse
        System.out.println(Pattern.matches(emailRegexp, "@b.com")); 
        System.out.println(Pattern.matches(emailRegexp, "a@b.c"));
          }
          
   
   
   //EJERCICIO 11---------------------------------------------------------------
    
   public static void main(String[] args) {
        EjerciciosExpresionesRegulares obj = new EjerciciosExpresionesRegulares();
        System.out.println("\nPIMER EJERCICIO\n");
        obj.main1();
        System.out.println("\nSEGUNDO EJERCICIO\n");
        obj.Ejercicio2();
        System.out.println("\nTERCER EJERCICIO\n");
        obj.Ejercicio3();
        System.out.println("\nCUARTO EJERCICIO\n");
        obj.main4();
        System.out.println("\nQUINTO EJERCICIO\n");
        obj.main5();
        System.out.println("\nSEXTO EJERCICIO\n");
        obj.main6();
        System.out.println("\nSEPTIMO EJERCICIO\n");
        obj.main7();
        System.out.println("\nOCTAVO EJERCICIO\n");
        obj.main8();
        System.out.println("\nNOVENO EJERCICIO\n");
        obj.main9();
        System.out.println("\nDECIMO EJERCICIO\n");
        obj.main10();
        System.out.println("\nONCEAVO EJERCICIO\n");
        obj.main11();
        }
    
}
