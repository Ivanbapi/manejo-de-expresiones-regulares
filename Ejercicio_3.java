import java.util.Scanner;

public class Ejercicio_3{
    
    public static void main(String[] args){
        Scanner s= new Scanner(System.in);
        
        System.out.println("Introduzca la palabra");
        String frase = s.nextLine();
        boolean validacion = false;
        
        for(int i=1; i<frase.length(); i++){
            if(frase.charAt(i-1)==frase.charAt(i)){
              validacion = true;
            }
            
        }
        System.out.println("La palabra es "+ validacion);
    }
    
}
